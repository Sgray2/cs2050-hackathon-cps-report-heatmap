__author__ = 'Sean'
import sqlite3
import json
import re
from flask import Flask, url_for

app = Flask(__name__)

response = sqlite3.connect("db.sqllite3")


def zipcode(data):
    curzip, count, total = 0, 1, 0
    by_zip = []
    for x in data:
        if (x[2] != ''):
            if (curzip == x[1]):
                total += float(re.sub("([^\d\.])", "", x[2]))

                count += 1
            else:
                by_zip.append({int(curzip): total / count})
                total = float(re.sub("([^\d\.])", "", x[2]))
                count = 1
                curzip = x[1]
    return by_zip


@app.route('/data.json')
def getstats():
    cur = response.cursor()
    suspend_high_sql = 'SELECT "Name of School", "Zip Code", "Suspensions per 100 2012" FROM CPS WHERE "Suspensions Per 100 2012" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(suspend_high_sql)
    suspend_data_high = cur.fetchall()
    elementry_sql = 'SELECT "Name of School", "Zip Code", "Suspensions Per 100 students 2012" FROM elementry WHERE "Suspensions Per 100 students 2012" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(elementry_sql)
    suspend_data_element = cur.fetchall()
    suspend_data_all = suspend_data_element + suspend_data_high
    # sort by zipcode
    suspend_data_all.sort(key=lambda tup: tup[1])
    print(suspend_data_all)
    return json.dumps(zipcode(suspend_data_all))

@app.route('/tattenddata.json')
def getTattendancestats():
    cur = response.cursor()
    teaatten_high_sql = 'SELECT "Name of School", "Zip Code", "Teacher Attendance Percentage 2013" FROM elementry WHERE "Teacher Attendance Percentage 2013" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(teaatten_high_sql)
    teaattn_data_high = cur.fetchall()
    teatten_element_sql = 'SELECT "Name of School", "Zip Code", "Teacher Attendance Percentage 2013" FROM elementry WHERE "Teacher Attendance Precentage 2013" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(teatten_element_sql)
    teaattn_data_element = cur.fetchall()
    teaattn_data_all = teaattn_data_element + teaattn_data_high
    teaattn_data_all.sort(key=lambda tup: tup[1])
    return json.dumps(zipcode(teaattn_data_all))

@app.route('/attenddata.json')
def getAttendancestats():
    cur = response.cursor()
    stuatten_high_sql =  'SELECT "Name of School", "Zip Code", "Student Attendance Percentage 2013" FROM CPS WHERE "Student Attendance Percentage 2013" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(stuatten_high_sql)
    stuatten_data_high = cur.fetchall()
    stuatten_element_sql = 'SELECT "Name of School", "Zip Code", "Student Attendance Percentage 2013" FROM elementry WHERE "Student Attendance Percentage 2013" IS NOT "" ORDER BY "Zip Code" ASC'
    cur.execute(stuatten_element_sql)
    stuatten_data_element = cur.fetchall()
    stuatten_data_all = stuatten_data_element+ stuatten_data_high
    stuatten_data_all.sort(key=lambda tup: tup[1])
    return json.dumps(zipcode(stuatten_data_all))

@app.route('/attenddata.json')
def getAttendancestats():
    cost_values= {'Very Strong':5,'Strong':4,'Neutral':3,'Week':2,'Very Week':1}
    cur = response.cursor()
    stuatten_high_sql =  'SELECT "Name of School", "Zip Code", "Involved Family" FROM CPS WHERE "Involved Family" IS NOT "Not Enough Data" ORDER BY "Zip Code" ASC'
    cur.execute(stuatten_high_sql)
    stuatten_data_high = cur.fetchall()
    stuatten_element_sql = 'SELECT "Name of School", "Zip Code", "Involved Family" FROM elementry WHERE "Involved Family" IS NOT "Not Enough Data" ORDER BY "Zip Code" ASC'
    cur.execute(stuatten_element_sql)
    stuatten_data_element = cur.fetchall()
    stuatten_data_all = stuatten_data_element+ stuatten_data_high
    stuatten_data_all.sort(key=lambda tup: tup[1])
    return json.dumps(zipcode(stuatten_data_all))


def numerateFamily(data){
    switch:
        case 'Very Strong':
            return 5; break;
}

if __name__ == "__main__":
   app.run()
