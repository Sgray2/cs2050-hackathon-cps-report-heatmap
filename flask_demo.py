from flask import Flask, url_for
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/data.json')
def get_json_data():
    return '{"a":"b"}'

if __name__ == "__main__":
    app.run()
